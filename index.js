'use strict';
const express = require('express');
const app = express();
const path = require('path');
const url = require('url');
const fs = require("fs");
const fallback = require('express-history-api-fallback');
const cookieParser = require('cookie-parser');
const session = require('express-session');

app.use(cookieParser());
app.use(session({
  key: 'JSESSIONID',
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}));
app.use(express.static(path.join(__dirname, 'dist')));
app.use(express.static(path.join(__dirname, 'jsonSpec')));
app.use(express.static(path.join(__dirname, 'Markup')));

// routes ======================================================================

app.use((req,res) => {
  req.session.name = 'shinhanroot';
  let filename = url.parse(req.url).pathname.replace('/','');
  if (filename === undefined) {
    filename = jsonSpec/index.html;
  }
  res.sendFile(filename, {root:__dirname});
});

app.get('/health', (req, res) => {
  res.send(new Buffer(JSON.stringify({
    pid: process.pid,
    memory: process.memoryUsage(),
    uptime: process.uptime(),
  })));
});

app.use(fallback(path.resolve(__dirname, 'pub/dashboard', 'index.html')));

// launch ======================================================================
const port = process.env.PORT || 8080;

app.listen(port, () => console.log(`The magic happens on port ${port}`));
